#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char password[PASS_LEN];
    char hashpass[33];
};

int sortFunc (const void *a, const void *b) {
    struct entry * aa = (struct entry *)a;
    struct entry * bb = (struct entry *)b;
    int result = strcmp(aa -> hashpass, bb -> hashpass);
    return result;
}

int searchFunc (const void *a, const void *b) {
    char * aa = (char *)a;
    struct entry * bb = (struct entry *)b;
    int result = strcmp(aa, bb -> hashpass);
    return result;
}

// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    // open file
    FILE * input = fopen(filename, "r+");
    if (!input) {
        perror("Can't open dictionary");
        exit(1);
    }

    // malloc memory for structs
    struct entry *entryArr = malloc(10 * sizeof(struct entry));

    // read line by line of input
    int count = 0;
    char buffer[PASS_LEN];
    int arrLen = 10;
    while(fgets(buffer, 100, input) != NULL) {
        // remove nl
        char *nl = strchr(buffer, '\n');
        if (nl) *nl = '\0';

        // realloc array to fit all
        if (count == arrLen) {
            arrLen += 5;
            entryArr = realloc(entryArr, arrLen * sizeof(struct entry));
        }

        // string malloc
        char *str = malloc(strlen(buffer) + 1);
        str = buffer;

        // copy string into struct
        strcpy(entryArr[count].password, str);

        // copy hash into struct
        char *hash = md5(str, strlen(str));
        strcpy(entryArr[count].hashpass, hash);

        count++;
    }

    *size = count;
    fclose(input);
    return entryArr;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int arrSize = 10;
    struct entry *dict = read_dictionary(argv[2], &arrSize);

    // Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, arrSize, sizeof(struct entry), sortFunc);

    // Open the hash file for reading.
    FILE * hashFile = fopen(argv[1], "r+");
    if (!hashFile) {
        perror("Can't open dictionary");
        exit(1);
    }

    char hashes[50];
    
    while (fgets(hashes, 50, hashFile) != NULL) {
        // remove nl
        char *nl = strchr(hashes, '\n');
        if (nl) *nl = '\0';

        struct entry *found = bsearch(hashes, dict, arrSize, sizeof(struct entry), searchFunc);

        printf("Password: %s, Hashed Password: %s\n", found -> password, found -> hashpass);

    }

    fclose(hashFile);
}